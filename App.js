import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import Navigation from './navigation/tab';
import { getFont } from "./constant/font"
import AppLoading from "expo-app-loading"
import { ScreenContext } from "./context"
import AsyncStorage from '@react-native-async-storage/async-storage';
// import  from ""
import AuthNavigator from "./navigation/stack/auth"

export default function App() {
  const [loading, setLoading] = useState(true)

  async function ope() {
    const bb = await AsyncStorage.getItem('@storage_Key')
    // alert(bb)
  }

  useEffect(() => {
    ope()
  }, [])
  if (loading) {

    return <AppLoading startAsync={getFont} onError={() => alert("Error occucured, close and restart.")} onFinish={() => setLoading(false)} />
  }

  return (
    <ScreenContext>
      {/* <AuthNavigator /> */}
      <Navigation />
    </ScreenContext>

  );
}


