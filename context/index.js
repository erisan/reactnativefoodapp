import React, { createContext, useContext } from "react"


const IsContext = createContext("")

export const ScreenContext = ({ children }) => {
    return (
        <IsContext.Provider value={{x: 1}}>
            {children}
        </IsContext.Provider>
    )
}
export default function appContext() {
    const data = useContext(IsContext)
    return data
}