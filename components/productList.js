import React, { useState } from 'react'
import { FlatList, View, Text, StyleSheet, TouchableOpacity, Image , ScrollView} from "react-native"
import { styles as S } from "../styles"
import { white } from "../constant/colors"


export default function ProductList({data , title}) {

    return (
        <View >
            {
                title &&
                <Text style={{ ...S.sub_title, marginTop: 20 }}>
                    {title}
            </Text>
            }

            <FlatList numColumns={2}  horizontal={false}  renderItem={({ item, index }) => {
                return (
                    <TouchableOpacity style={{ flex: 1 , marginTop: 15, marginRight: index % 2 ==0 ? 10 : 0 ,marginLeft: index % 2 !==0 ? 10 :0 }}>

                        <Image source={item.image} style={{ width: "100%", height: 150, borderRadius: 15 }} resizeMode="cover" />
                        <View style={styles.summaryConatiner}>
                            <Text style={S.body1}>
                                {item.name}
                            </Text>
                            <Text style={{ fontFamily: "proximaNova", color: "#444", fontSize: 12, marginTop: 3 }}>
                                {item.duration}
                            </Text>
                        </View>
                    </TouchableOpacity>
                )
            }} keyExtractor={(props, index) => props.name + index} data={data} />

        </View>
    )
}



const styles = StyleSheet.create({
    summaryConatiner: {
        position: "absolute",
        backgroundColor: "#fff",
        bottom: 0,
        padding: 10,
        borderTopRightRadius: 15,
        borderBottomLeftRadius: 15,

    }
})