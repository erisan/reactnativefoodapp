import React, { useState } from 'react'
import { FlatList, View, Text, StyleSheet, TouchableOpacity , Image , Dimensions} from "react-native"
import { styles as S } from "../styles"
import { white } from "../constant/colors"




const windowidth= Dimensions.get("window").width

const getPercentOfWindowWidth = (percent) =>  ( percent / 100) * windowidth

export default function Popular({ data , navigation }) {

    const handlePress = (item) => {
        navigation.navigate("Product details" , {data: {...item}})
    }


    return (
        <View >
            <Text style={{ ...S.sub_title , marginTop: 20}}>
                Popular
            </Text>

            <FlatList showsHorizontalScrollIndicator={false} horizontal={true} renderItem={({ item  , index}) => {
                return (
                    <TouchableOpacity onPress={handlePress.bind(this , item)} style={{ paddingRight:data.length !== index + 1 ? 20 : 0, marginTop: 15 }}>
                       
                        <Image source={item.image} style={{ width: getPercentOfWindowWidth(70), height: 150, borderRadius: 15 }} resizeMode="cover" />
                        <View style={styles.summaryConatiner}>
                            <Text style={S.body1}>
                                {item.name}
                            </Text>
                            <Text style={{ fontFamily: "proximaNova", color:  "#444" , fontSize: 12, marginTop: 3 }}>
                                {item.duration}
                            </Text>
                        </View>
                    </TouchableOpacity>
                )
            }} keyExtractor={(props , index)=> props.name + index} data={data} />
            
      </View>
    )
}



const styles = StyleSheet.create({
    summaryConatiner: {
        position: "absolute",
        backgroundColor: "#fff",
        bottom: 0,
        padding: 10,
        borderTopRightRadius: 15,
        borderBottomLeftRadius: 15,

    }
})