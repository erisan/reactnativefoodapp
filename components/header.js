import React from 'react'
import { View, Text,  SafeAreaView, StatusBar, Platform } from "react-native"
import { styles} from "../styles"
import { Ionicons } from '@expo/vector-icons';


export default function Header() {
    return (
        <SafeAreaView>
            <View style={styles.headerContainer}>
                <View style={styles.yCenter}>
                    <Ionicons name="locate" size={24} color="black" />
               </View>
                <View style={{ backgroundColor: "#e7e5e3", alignItems: "center" ,justifyContent: "center",  borderRadius: 20, width: "40%" ,}}>
                    <Text style={styles.body1} >17 lincoin PI</Text> 
                </View>
                <View style={styles.yCenter}>
                    <Ionicons name="fast-food-outline" size={24} color="black" />
                </View>
            </View>
      </SafeAreaView>
    )
}
