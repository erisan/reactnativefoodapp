import React, { useState } from 'react'
import data from "../constant/data"
import { FlatList, View, Text, StyleSheet, TouchableOpacity } from "react-native"
import { styles as S } from "../styles"
import { white } from "../constant/colors"
const { categories } = data()

export default function CategoriesTemplate({data}) {

    const [activeCategory, setActiveCategory] = useState(0)

    const handlePress = (incomingData) => {
        setActiveCategory(incomingData.index)
         
    }

    return (
        <View style={{marginTop: 10}}>
            <FlatList
                data={data}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(props, index) => props.name + index}
                horizontal={true}
                renderItem={({ item, index }) => {
                    let active = index == activeCategory
                    return (
                        <TouchableOpacity onPress={handlePress.bind(this, { index, ...item })} style={{ ...styles.categoryContainer, paddingRight: data.length !== index + 1 ? 20 : 0}}>
                            <View style={{ ...styles.categoryContentWrapper, ...S.xCenter, ...S.body1, backgroundColor: active ? "orange" : "#fff" }}>
                                <View style={{ ...styles.categoryIconContainer, ...S.xAndYcenter, backgroundColor: active ? "#fff" : "#eee" }}>
                                    <Text>
                                        {item.icon}
                                    </Text>
                                </View>

                                <Text style={{ fontFamily: "proximaNova", color:active ? "#fff" : "#444"}}>
                                    {item.name}
                                </Text>
                            </View>

                          

                        </TouchableOpacity>
                    )
                }}

            />
        </View>
    )
}


const styles = StyleSheet.create({
    categoryContainer: {
        width: 95,
        
    },
    categoryIconContainer: {
        width: 40,
        height: 40,
        marginBottom: 10,
        borderRadius: 200,
    },
    categoryContentWrapper: {
        borderRadius: 30,
        paddingVertical: 15,
        alignItems: "center"
    }
})

