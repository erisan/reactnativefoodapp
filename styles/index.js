
import { StyleSheet, Platform, StatusBar } from "react-native"
import {primary  , secondary} from "../constant/colors"


// get platform details

const isOs = Platform.OS
const isVersion = Platform.Version

const phoneIsIphone = isOs === "iphone"



export const styles = StyleSheet.create({
    headerContainer: {
        marginTop: phoneIsIphone && isVersion >= 11 ? 0 : StatusBar.currentHeight +  10,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 20
    },
    yCenter: {
        alignItems: "center",
    },
    xCenter: {
        justifyContent: "center"
    },
    xAndYcenter: {
        alignItems: "center",
        justifyContent: "center",
    },
        pageWrapper: {
            padding: 20,
        }
    ,
    body1: {
        fontFamily: "proximaNovaBold",
        color: primary,
        // fontWeight: "bold",
        fontSize: 14,
    },
    body2: {
        fontFamily: "proximaNovaBold",
        color: "#333",
        // fontWeight: "bold",
        fontSize: 15,
    },
    title: {
        fontSize: 25,
        fontFamily: "recoleta-bold",

    },
    sub_title: {
        fontSize: 20,
        fontFamily: "recoleta-bold",

    }
})