import React from "react"
import { StyleSheet, SafeAreaView, Text, View, ScrollView, FlatList } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Header from "../components/header"
import { styles } from "../styles"
import CategoriesTemplate from "../components/categories";
import Popular from "../components/popular";
import data from "../constant/data"
import ProductList from "../components/productList";

const { popular, categories } = data()

function VirtualizedView(props) {
    return (
        <FlatList
            data={[]}
            ListEmptyComponent={null}
            showsVerticalScrollIndicator={false}
            keyExtractor={() => "dummy"}
            renderItem={null}
            ListHeaderComponent={() => (
                <>
                    <View style={{marginBottom: 240}}>
                         {props.children} 
                    </View>
                  
                </>
            )}
        />
    );
}

export default HomeScreen = ({ navigation }) => (
    <>
        <Header />
        <View style={{ ...styles.pageWrapper }}>
            <View style={{ marginTop: 15 }}>
                <Text style={styles.title}>Fast & Delicious</Text>
                <Text style={styles.title}> Food</Text>
            </View>
            <CategoriesTemplate data={categories} />




            <VirtualizedView>
                <View>
                    <Popular navigation={navigation} data={popular} />
                </View><View>
                    <ProductList title="All" data={popular} />
                </View>
            </VirtualizedView>


        </View>
    </>
)