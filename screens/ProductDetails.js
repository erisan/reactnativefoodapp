import React, { useState, useEffect, useRef } from 'react'
import { View, Text, SafeAreaView, useWindowDimensions, FlatList, Animated, StatusBar, Platform, StyleSheet, Image, TouchableOpacity } from "react-native"
import { styles as S } from "../styles"
import { AntDesign } from '@expo/vector-icons';
import data from "../constant/data"
import appContext from "../context"
const { popular } = data()



const Pagination = ({ active, data, scrollPosition }) => {
    console.log("active", active)
    const { width } = useWindowDimensions()
    if (!width) return
    return (
        <View style={{ flexDirection: "row", justifyContent: "center", alignSelf: "center", position: "absolute", top: 300 }}>
            {/* <Text>
            This is a text
        </Text> */}
            {
                data?.map((_data, _index) => {

                    const inputRange = [(_index - 1) * 300, _index * 300, (_index + 1) * 300]

                    {/* const width = scrollPosition.interpolate({
                        inputRange,
                        outputRange: [10, 25, 10],
                        extrapolate: "clamp"
                    }) */}
                    {/* const background = scrollPosition.interpolate({
                        inputRange,
                        outputRange: ["transparent", "#fff", "transparent"],
                        extrapolate: "clamp"
                    }) */}
                    return (
                        <Animated.View key={_index} style={{ borderRadius: 40, width: 20, height: 15, marginHorizontal: _index == 1 ? 3 : 0, borderWidth: 2, borderColor: "#fff", backgroundColor: active.index == _index ? "#fff" : "transparent" }}>
                        </Animated.View>
                    )
                })
            }
        </View>
    )
}
export default function ProductDetails({ route, navigation }) {

    const screenWidth = useWindowDimensions().width
    const scrollPosition = useState(new Animated.Value(0))[0]
    const [active, setActive] = useState({})
    const [data, setData] = useState(null)


    const viewConfig = { viewAreaCoveragePercentThreshold: 50 }

    useEffect(() => {
        let isFilter = popular.filter(item => item.index == route.params.data.index)

        setData({ data: isFilter[0], images: [isFilter[0].image, isFilter[0].image1, isFilter[0].image2,] })
    }, [route.params])

    const handleOviewAble = ({ viewableItems}) => {
        if (viewableItems[0].index) {
            setActive({ ...active, index:viewableItems[0].index })
        }
    }


    return (
        <>
            <View style={{ ...S.headerContainer, paddingHorizontal: 0, flex: 1 }}>
                <View style={{ ...S.headerContainer, ...styles.arrowContainer }}>
                    <TouchableOpacity style={{ ...S.xAndYcenter, width: 40, height: 40, borderRadius: 100, backgroundColor: "#fff" }} onPress={() => navigation.goBack()}>
                        <AntDesign name="arrowleft" size={24} color="black" />
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1, flexDirection: "column" }}>
                    {/* <Text>ddd</Text> */}
                    {
                        data?.images &&

                        <FlatList keyExtractor={(item, index) => index}
                            numColumns={1}
                            horizontal
                            pagingEnabled
                            showsHorizontalScrollIndicator={false}
                            onViewableItemsChanged={handleOviewAble}
                            viewabilityConfig={viewConfig}
                            data={data.images}
                            bounces={false}
                            onScroll={Animated.event([{
                                nativeEvent: { contentOffset: { x: scrollPosition } }
                            }], { useNativeDriver: false })}

                            renderItem={({ item }) => (
                                <View>
                                    <Image style={{ width: screenWidth, height: 350 }} source={item} />
                                    <View style={{ flex: 1 }}>
                                        <View style={{ position: "absolute", top: -50, backgroundColor: "#eee", paddingBottom: 40, borderTopRightRadius: 40, borderTopLeftRadius: 40, width: screenWidth }}>
                                            <View style={{ paddingHorizontal: 20, flexDirection: "column", justifyContent: "center" }}>
                                                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 25 }}>
                                                    <Text style={{ ...S.sub_title }}>{data.data.name}</Text>
                                                    <TouchableOpacity>
                                                        <AntDesign name="pluscircle" size={24} color="black" />
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{ flexDirection: "column", marginTop: 10 }}>
                                                    <Text style={{ ...S.body2, marginTop: 15 }}>{data.data.description}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>


                                </View>
                            )
                            }

                        />
                    }
                </View>
            </View>
            <Pagination active={{ ...active }} scrollPosition={scrollPosition} data={data?.images} />
        </>

    )
}


const styles = StyleSheet.create({
    arrowContainer: {
        position: "absolute",
        zIndex: 4,
        top: -25
    }
})

