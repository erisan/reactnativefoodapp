
import { Ionicons, FontAwesome5, MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import React from "react"

export default function data() {
    const categories = [
        {
            name: "All",
            icon: <MaterialCommunityIcons name="forwardburger" size={24} color="orange" />,
        },
        {
            name: "Pizza",
            icon: <Ionicons name="ios-pizza-outline" size={24} color="orange" />,
        },
        {
            name: "Burger",
            icon: <FontAwesome5 name="hamburger" size={24} color="orange" />,
        },
        {
            name: "Shawarma",
            icon: <MaterialCommunityIcons name="sausage" size={24} color="orange" />,
        },
        {
            name: "Pizza",
            icon: <Ionicons name="ios-pizza-outline" size={24} color="orange" />,
        },
        {
            name: "Burger",
            icon: <FontAwesome5 name="hamburger" size={24} color="orange" />,
        },
        {
            name: "Shawarma",
            icon: <MaterialCommunityIcons name="sausage" size={24} color="orange" />,
        }
    ]

    const popular = [
        {
            index: 1,
            name: "Large x2 Pizza",
            image: require("../assets/img/pizza.jpg"),
            image1: require("../assets/img/pizza.jpg"),
            image2: require("../assets/img/pizza.jpg"),
            duration: "10-15 min",
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
        },
        {
            index: 2,
            name: "Burger to dun",
            image: require("../assets/img/burger.jpg"),
            image1: require("../assets/img/burger.jpg"),
            image2: require("../assets/img/burger.jpg"),
            duration: "7-10 min",
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"

        },
        {
            index: 3,
            name: "Shawarma x1 Long",
            image: require("../assets/img/shawarma.jpeg"),
            image1: require("../assets/img/shawarma.jpeg"),
            image2: require("../assets/img/shawarma.jpeg"),
            duration: "20-25 min",
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"

        },
    ]

    return ({
        categories,
        popular
    })

}