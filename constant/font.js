import React from 'react'
import * as Font from "expo-font"


export const getFont = () => {
    return Font.loadAsync({
        "recoleta": require("../assets/font/recoleta/Recoleta-Regular.ttf"),
        "recoleta-bold": require("../assets/font/recoleta/Recoleta-Bold.ttf"),
        "proximaNova": require("../assets/font/ProximaNova/ProximaNova-Regular.ttf"),
        "proximaNovaBold": require("../assets/font/ProximaNova/ProximaNova-Bold.ttf"),

        
        
    })
}