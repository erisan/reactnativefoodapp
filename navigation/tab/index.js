import React from "react"
import { TouchableOpacity, View, Text, StyleSheet } from "react-native"
import { NavigationContainer } from "@react-navigation/native"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { Ionicons, AntDesign, MaterialIcons } from '@expo/vector-icons';
import HomeStack from "../stack"



const Tab = createBottomTabNavigator()


const Menu = () => {

    const TabBarCustomButton = ({ accessibilityState, children, ...props }) => {
        let isSelected = accessibilityState.selected

        if (isSelected) {
            return (
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <View style={styles.activeMenu}>
                        <View style={styles.activeChild}>
                            {children}
                      </View>
                    </View>
                </View>
            )
        }
        else {
            return (
                <TouchableOpacity {...props} >
                    {children}
                </TouchableOpacity>
            )
        }

    }

    return (
        <NavigationContainer>
            <Tab.Navigator
                screenOptions={({ route }) => {
                    return ({
                        headerShown: false,
                        tabBarShowLabel: false,
                        tabBarButton: ({ ...props }) => {
                            return <TabBarCustomButton {...props} />
                        },
                        tabBarIcon: ({ focused, color }) => {
                            switch (route.name) {
                                case "Home": return <Ionicons name="fast-food-outline" size={24} color={color} />;
                                    break;
                                case "Search": return <AntDesign name="search1" size={24} color={color} />;
                                    break;
                                case "Favorite": return <MaterialIcons name="favorite-border" size={24} color={color} />
                                    break;
                                case "Profile": return <Ionicons name="people-outline" size={24} color={color} />;
                                    break;
                            }
                        },
                        tabBarInactiveTintColor: "#555",
                        tabBarActiveTintColor: "orange"
                    })
                }}>
                <Tab.Screen name="Home" component={HomeStack} />
                <Tab.Screen name="Search" component={HomeStack} />
                <Tab.Screen name="Favorite" component={HomeStack} />
                <Tab.Screen name="Profile" component={HomeStack} />


            </Tab.Navigator>
        </NavigationContainer>
    )
}


const styles = StyleSheet.create({

    activeMenu:
    {
        position: "absolute",
        top: -20,
        flex:1,
        
    }

    ,
    activeChild: {
        backgroundColor: "#fff",
        width: 40, height: 40,
        borderRadius: 50,
        borderWidth: 20,
        borderColor: "#eee",
    }


})

export default Menu