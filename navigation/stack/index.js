
import React from "react"
import { createStackNavigator } from "@react-navigation/stack"
import HomeScreen from "../../screens/HomePage"
import ProductDetails from "../../screens/ProductDetails"


const Stack = createStackNavigator()

const HomeStack = () => (
    <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Product details" component={ProductDetails} />
        
    </Stack.Navigator>
)


export default HomeStack