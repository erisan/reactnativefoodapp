
import React from "react"
import { NavigationContainer } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"


import Login from "../../../screens/auth/login"
import Signup from "../../../screens/auth/signup"

const Auth = createStackNavigator()

export default function index() {
    return (
        <NavigationContainer>
            <Auth.Navigator screenOptions={{headerShown: false}}>
                <Auth.Screen name="Login"component={Login} />
                <Auth.Screen name="Signin" component={Signup}  />
            </Auth.Navigator>
       </NavigationContainer>
    )
}
